---
Categories:
    - Flow
Tags:
    - OKR
    - Conference
    - Atelier
    - Jeu
    - Agilemans
date: 2022-03-03
title: OKR à Springfield - l'atelier 
images:
    - board-okr.jpg
cover:
    description: Photo du tableau des OKRs
    brightness: 50%
---

Tout à commencé l'année dernière par une commande de collectivités territoriales : "Nous souhaitons un atelier ludique pour comprendre les OKR". Je me suis donc attelé à la tache de construire un jeu sérieux pour découvrir les OKR. Convaincu que le travail collectif est mille fois plus efficace que le travail solitaire, j'ai commencé à regrouper quelques personnes autour de moi pour travailler sur ce jeu, en particulier [Jacques Prou](https://www.linkedin.com/in/jacques-prou-1268821/) et [Anne Gabrillagues](https://www.linkedin.com/in/agabrillagues/). Il en résulte un jeu sous licence presque libre, tout est libre, sauf les illustrations qui restent la propriété exclusive de Matt Groening, en effet, tout cet atelier se déroule dans l'univers des Simpson.

Aujourd'hui j'étais invité à animer cet atelier dans un cadre que j'apprécie particulièrement : [AgiLeMans](https://agilemans.org/) l'événement annnuel agile au Mans. Organisé par une équipe absolument géniale pleine d'énergie et d'amour. 

L'idée c'est d'expérimenter dans un cadre ludique :
* la construction des OKRs tactiques par les équipes, 
* l'alignement les équipes entre elles pour éviter les optimums locaux
* l'alignement verticale
* et le suivi des OKRs sur quelques itérations.

Je pense avoir atteint mon objectif, en effet, à la clôture de l'atelier j'ai fait un cercle de parole pour avoir l'avis de tous les participants. Je vous en livre 2 qui illustrent le propos générale :
* C'était super, j'ai l'impression d'avoir tout compris de ce que sont les OKRs et des principales difficultés pour leurs mise en œuvre.
* C'était génial, nous avons bien rigolé et je crois que je retiendrais longtemps ce que sont les OKRs et pourquoi l'alignement horizontal est si important que ça.

Avis aux organisateurs d'événements agile, j'ai un atelier que les participants trouvent fun pour découvrir les OKR. Pour celles et ceux qui veulent essayer de le jouer, tout est là : https://gitlab.com/azae/jeux/okr-springfield
