---
Categories:
    - Enseignement
Tags:
    - TDD
    - software craftsmanship
date: 2021-12-11
title: Un sujet de DS très TDD
images:
    - pagination.png
cover:
    description: Pagination
    brightness: 40%
---

Proposer un sujet pour valider la bonne compréhension du TDD à des étudiants qui vont composer sur papier n'est pas évident. Dans le dernier sujet de DS que nous avons élaboré (mes collègues et moi même) il y avait cet exercice sur 5 points.

## Le sujet
Pour faire apparaître les possibilités de navigation dans un long texte, on peut utiliser les notions de
première page, page précédente, page courante, page suivante, et dernière page.
Une proposition d’affichage de ces options de manière concise est la « pagination seven » qui utilise
au plus 7 positions.
Pour des petits documents (7 pages ou moins), on affiche tous les numéros de pages :

    1 (2) 3 4 5

indiquant qu’on est sur la page 2 et qu’on peut accéder (par clic) aux pages de 1 à 5

Si on est sur une page quelconque d’un document long (plus de 7 pages), on met des ellipses :

    1 ... 4 (5) 6 ... 42

indiquant qu’on est sur la page 5 et qu’on peut accéder à la première page, la dernière (p. 42), et les
pages précédente et suivante de la courante.

Si   on  est   au   début   ou  à   la   fin   d’un  document   long,   on  ne   met   qu’une  ellipse  (toujours   avec   7
éléments en comptant l’ellipse) :

    1 ... 38 (39) 40 41 42

indiquant qu’on est sur la page 39 d’un document de 42 pages.


Étant   donné   une   fonction  String page7(int courante, int total)  qui   prend   en
paramètre   le   numéro   de   page   courant   et   le   nombre   de   pages   total   et   qui   retourne   la   string
représentant la « pagination seven » correspondante, rédigez et ordonnez les tests que vous feriez
dans une approche TDD. Pour chaque test, donnez l'intention de code qu’il fait émerger.

## Une solution
Voilà ce que je m'attendait à avoir comme proposition de solution : 

|      Entrée   | Affichage attendu   | Intention de code |
|---------------|---------------------|-------------------|
| page7(1,1)    | (1)                                               | Traitement minimum, une seule page |
| page7(2,2)    | 1&nbsp;(2)                                        | Afficher différemment la page courante, premier if |
| page7(1,2)    | (1)&nbsp;2                                        | le cas précédent était probablement codé en dure, maintenant on doit avoir un vrai if |
| page7(5,5)    | 1&nbsp;2&nbsp;3&nbsp;4&nbsp;(5)                   | plusieurs pages à gauche, introduction d'une boucle |
| page7(1,7)    | (1)&nbsp;2&nbsp;3&nbsp;4&nbsp;5&nbsp;6&nbsp;7     | plusieurs pages à droite pour vérifier que la boucle mache bien |
| page7(4,7)    | 1&nbsp;2&nbsp;3&nbsp;(4)&nbsp;5&nbsp;6&nbsp;7     | vérifier que ça fonctionne avec les boucles avant et après la page courante |
| page7(10,13)  | 1&nbsp;…&nbsp;9&nbsp;(10)&nbsp;11&nbsp;12&nbsp;13 | Ellipse à gauche, introduction d'une nouvelle boucle |
| page7(12,13)  | 1&nbsp;…&nbsp;9&nbsp;10&nbsp;11&nbsp;(12)&nbsp;13 | Ellipse à gauche et maintient de 7 éléments, il faut probablement ajouter une boucle pour revenir sur les chiffres à afficher à gauche |
| page7(4,42)   | 1&nbsp;2&nbsp;3&nbsp;(4)&nbsp;5&nbsp;…&nbsp;42    | Ellipse à droite, introduction de la condition et/ou de la boucle correspondante |
| page7(1,42)   | (1)&nbsp;2&nbsp;3&nbsp;4&nbsp;5&nbsp;…&nbsp;42    | Ellipse à droite et maintient de 7 éléments, introduction de la même logique que pour l'ellipse à gauche et le maintient des 7 éléments |
| page7(7,42)   | 1&nbsp;…&nbsp;6&nbsp;(7)&nbsp;8&nbsp;…&nbsp;42    | Double ellipse, il est peut-être préférable d'introduire ce test avant le maintient du nombre d'éléments |


