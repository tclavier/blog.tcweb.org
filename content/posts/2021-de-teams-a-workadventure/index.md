---
Categories: 
    - Coup de gueule
Tags:
    - logiciel libre
    - visio
    - workadventure
date: 2021-04-02
title: De Teams à WorkAdventure
images: 
    - workadventure-cover.png
cover:
    description: Screenshot WorkAdventure
    brightness: 50%
aliases:
    - /post/de-teams-a-workadventure
    - /article/de-teams-a-workadventure
    - /articles/de-teams-a-workadventure
---

Voici clé en main le message que j'ai construit pour essayer de convaincre un RSSI d'accépter [WorkAdventure](https://workadventu.re/) comme alternative complémentaire à [Teams](https://www.microsoft.com/fr-fr/microsoft-teams/).

<!--more-->

> Bonjour,
> 
> 
> Je me permets de vous solliciter afin de référencer [WorkAdventure](https://workadventu.re/) comme alternative complémentaire à Teams.
> 
> WorkAdventure est un environnement de visioconférence ludique qui reproduit dans une sorte de jeu vidéo les interactions possibles physiquement.
> 
> On retrouve entre autres :
> 
> * Créer dynamiquement de petits groupes de travail.
> * Laisser la liberté à chacun de changer de groupe très facilement.
> * Repasser très simplement en grand groupe.
> * Avoir des échanges audios et vidéos informels.
> 
> Pour résumer, on retrouve tout un ensemble de leviers couramment utilisés durant les rituels agile et les conférences pour décupler les pouvoirs de l'intelligence collective.
> 
> WorkAdventure et un logiciel libre développé par la société française The Coding Machine : https://github.com/thecodingmachine/workadventure.
> 
> L'université de Lille ayant eu l'intention de rajouter WorkAdventure à la liste de leurs logiciels de visioconférence, tout le code a été analysé en janvier par le laboratoire d'informatique de l'université de Lille. Il en ressort les éléments suivants :
> 
> * Il est très facile à déployer sur sa propre infrastructure ;
> * Les connections sont anonymes ;
> * Aucune donnée n'est sauvegardée par le logiciel (tout est géré par le navigateur) ;
> * Il y a 2 types de visioconférence ;
>     * Des visios de groupes dans des zones définis de la carte ;
>     * Des visios informelles avec 3 personnes maximum en webrtc de pair à pair ;
> * L'ensemble des visioconférences de groupes sont chiffrées de bout en bout sans aucun moyen d'en intercepter un seul élément ;
> * Les visioconférences informelles ne sont pas systématiquement chiffrées de bout en bout, mais ne transitent pas par le serveur ;
> * L'ensemble des échanges écrits partagés dans les visioconférences de groupes sont chiffrés de bout en bout et non enregistrés sur le serveur ;
> * Les échanges écrits dans les visioconférences informelles sont quant à eux non chiffrés, et volatiles (non enregistré sur le serveur).
> 
> En comparaison,
> 
> * Microsoft a annoncé en mars 2021 un début de déploiement de la vidéo conférence chiffrée de bout en bout dans Teams pour le second semestre 2021 et uniquement dans certaines conditions.
> * Microsoft est soumis à la loi patriot act qui autorise l'ensemble des autorités américaines à collecter et analyser l'ensemble des données de tous leurs clients. Ce qui est d'ailleurs source de nombreuses fuites de données.
> * Le code de Microsoft Teams n'est pas auditable par une entité neutre et indépendante de Microsoft.
> * Microsoft conserve l'ensemble des éléments échangés dans Teams.
> * Les OneNote sont chiffrés à froid avec un secret partagé accessible aux administrateurs.
> * Les éléments SharePoint sont quant à eux chiffrés à froid uniquement s'ils ont été paramétrés de la sorte. Là encore avec un secret partagé accessible aux administrateurs.
> 
> Dans le cadre de BlaBlaBla, nous aimerions pouvoir utiliser ce logiciel afin de proposer une expérience nouvelle et bien plus riche que la dernière fois à l'ensemble des acteurs du développement logiciel de BlaBlaBla.
> 
> J'attire de plus, votre attention, sur le fait que de nombreux trains ont montré de l'intérêt pour cet outil afin d'enrichir les PI planning et les cérémonies d'Inspect and Adapt, ces événements que la crise sanitaire actuelle nous oblige à faire à distance.
> 
> Merci d'avance.
