---
date: 2021-04-04
title: À propos de Thomas Clavier
images: 
    - avatar.svg
cover:
    description: Avatar Thomas Clavier
    brightness: 50%
---

Je suis Thomas Clavier, entrepreneur, coach “ce que vous voulez” chez [aqoba](https://aqoba.fr) et enseignant chercheur à l’université de Lille. Mes sujets de prédilection sont l’agilité, devops, docker, le lean startup, l’artisanat logiciel, l’excellence opérationnelle. Mes clients m’appellent pour les aider à avoir des équipes qui délivrent plus, plus vite et plus sereinement. 
Je suis convaincu que le bonheur au travail n’est pas un luxe, mais une priorité et que la productivité n’est qu’un effet de bord de ce dernier. Pour l’instant, toutes mes expériences confirment cette conviction.

Depuis 2000, j’essaye de transformer le travail en un jeu, faire progresser les geeks, les développeurs et les managers, poser des questions pour aider les équipes dans leur amélioration continue. Questionner la direction, pour les aider à voir ce que le système leur cache, les aider à prendre les bonnes décisions pour faire progresser leurs équipes.

En 2011 j'ai créé la société [Azaé](https://azaé.net), j'ai cherché à construire une entreprise démocratique avec une organisation très largement inspiré des communautés du libre de l'agilité et des entreprises libérées. Aujourd'hui elle vit sans moi. Aux dernières nouvelles, elle est encore démocratique mais a totalement perdu son implication dans le logiciel libre.

J’aime particulièrement la gastronomie, le logiciel libre, Vim et les trolls techniques. Pour moi, le beau code n’est pas une option, et la course à pied en pleine nature une activité hebdomadaire. Pour me rencontrer, rendez-vous dans le train entre Paris et Lille, dans les trails du nord, dans des meetups et des conférences sur l’agilité et le software craftsmanship.

Mon matériel pédagogique préféré, c’est le Légo, grâce à lui, j’ai fait comprendre à des directions, des enseignants (y compris des enseignants de Français) et des business analystes ce qu’étaient le développement dirigé par les tests (TDD) et le software craftsmanship.

Vous pouvez me suivre ou me joindre sur de nombreux canaux : 

<div style="display: flex; align-items: center; justify-content: center;">

| | |
|-|-|
|<i class="fa fa-envelope"></i>   | tom(chez)tcweb.org |
|<i class="fa fa-gitlab"></i>     | https://gitlab.com/tclavier/ |
|<i class="fa fa-linkedin"></i>   | https://linkedin.com/in/thomasclavier/ |
|<i class="fa fa-mastodon"></i>   | [@thomas@pleroma.tcweb.org](https://pleroma.tcweb.org/thomas) |
|<i class="fa fa-matrix-org"></i> | [@tclavier:matrix.org](https://matrix.to/#/@tclavier:matrix.org) |
|<i class="fa fa-peertube"></i>   | https://video.ploud.fr/accounts/thomas/ |
|<i class="fa fa-phone"></i>  <i class="fa fa-signalapp"></i>    | +33 6 20 81 81 30 |
|<i class="fa fa-twitter"></i>    | [@thomasclavier](https://twitter.com/thomasclavier) |

</div>

Enfin vous trouverez ma clé publique gpg sur les serveurs [SKS](https://spider.pgpkeys.eu/) sous l'identifiant [20CCE23E53E6E41A](https://keyserver.ubuntu.com/pks/lookup?search=20CCE23E53E6E41A&fingerprint=on&op=index)
