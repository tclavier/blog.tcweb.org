---
date: 2022-02-05
title: Soutenir l’auteur
images: 
    - money.jpg
cover:
    description: Pièces de monaie
    brightness: 50%
---

Ce blog est disponible à prix libre, c'est à dire qu'il est payant mais vous pouvez en choisir le prix.

## Pourquoi ce blog est-il payant ?

Tout travail ne mérite pas forcément un salaire, mais :

💰  L'hébergement à un coût, que ce soit en terme de machine ou de noms de domaines.

🧠  Parce que je souhaite expérimenter des formes alternatives de rémunération **sans publicité**. Être dans la théorie, c’est bien mais la moindre des choses c'est de mettre en pratique ce que je préconise.

💗  Parce que plus j’ai d’argent, plus j’en donne aux créateurs, aux artistes et aux projets que j’aime.

## Comment me soutenir ?

<i class="fa fa-heart red"></i> En soutenant des logiciels libres. N’oubliez pas de m’envoyer un email car je n’ai aucun moyen de vous contacter pour vous dire merci.

<i class="fa fa-liberapay"></i> Par Leberapay, en utilisant le lien suivant : https://liberapay.com/tclavier/. N’oubliez pas de m’envoyer un email car je n’ai aucun moyen de vous contacter pour vous dire merci.

<i class="fa fa-money"></i> Par Lydia, en utilisant le numéro de téléphone +33 6 20 81 81 30. N’oubliez pas de m’envoyer un email car je n’ai aucun moyen de vous contacter pour vous dire merci.

<i class="fa fa-bank"></i> Par virement banquaire sur le compte IBAN FR76 1679 8000 0100 0114 7130 366 (BIC: TRZOFR21XXX). Pratique si vous êtes dans la zone SEPA. Par contre, n’oubliez pas de m’envoyer un email car je n’ai aucun moyen de vous contacter pour vous dire merci. Attention, certaines banques ne semblent pas respecter la directive SEPA et facturent des frais importants. Renseignez-vous !

<i class="fa fa-bitcoin"></i> Par Bitcoin, à l’adresse 3AJJqo6m9udGxvpBgQ33xLnuEdMY1jqX5e. Idéal et 100% sans frais. N’oubliez pas de m’envoyer un email car je n’ai aucun moyen de vous contacter pour vous dire merci.

<i class="fa fa-paypal"></i> Par Paypal, en utilisant l’adresse tom@tcweb.org. Attention: pour les petites sommes, les frais peuvent être prohibitifs (de 0 à 30%).

## Mais encore ...

Merci de partager et colporter mes contenus sur les réseaux sociaux. Cela permet à vos amis de me découvrir et cela ne vous coûte qu’un clic. N’hésitez donc pas à partager !


