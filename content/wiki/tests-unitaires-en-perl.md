+++
date = "2012-07-31"
draft = false
title = "Tests unitaires en perl"
color = "pink"
icon = "bell-slash-o"
Tags = ["tdd", "perl"]
description = "Des Tests unitaires en perl sans module"
+++

Je ne parlerais pas du modèle classique d'organisation du code en Perl :

-   toute la logique dans un module
-   un ou des scripts qui ne font qu'utiliser le module
-   les tests qui font eux aussi appel au module

Ce qui implique d'avoir au moins 3 fichiers différents ...

Dans mon cas, j'ai un simple script perl que je souhaite écrire en TDD.

mon\_script.pl

    #!/usr/bin/perl -w
    use strict;
    use warning;

    sub run {
      return "OK";
    }

    run() unless caller();
    1;

mon\_script.t

    #!/usr/bin/perl -w
    use strict;
    use warnings;
    use Test::More;
    require "./mon_script.pl";

    my $ret = run();
    is($ret, "OK");
    is($ret, "KO");


    done_testing()

Lancer `mon_script.t` pour voir le résultat des tests :

    ok 1
    not ok 2
    #   Failed test at ./mon_script.t line 8.
    #          got: 'OK'
    #     expected: 'KO'
    1..2
    # Looks like you failed 1 test of 2.


Liens
-----

<http://lampzone.wordpress.com/2012/04/03/pbp-part2-unit-test-your-perl-script/>
