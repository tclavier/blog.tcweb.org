==================================================================
https://keybase.io/tclavier
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://blog.tcweb.org
  * I am tclavier (https://keybase.io/tclavier) on keybase.
  * I have a public key with fingerprint 9FDD 8A0D 5E2F 93A1 3D1D  B8F2 20CC E23E 53E6 E41A

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101c8805e2cab7ad700d950c87733525c5290a4113d0a09415314f5e481bddbe00e0a",
      "fingerprint": "9fdd8a0d5e2f93a13d1db8f220cce23e53e6e41a",
      "host": "keybase.io",
      "key_id": "20cce23e53e6e41a",
      "kid": "0101c8805e2cab7ad700d950c87733525c5290a4113d0a09415314f5e481bddbe00e0a",
      "uid": "cab200c19bad10d815e920bf131d2f19",
      "username": "tclavier"
    },
    "service": {
      "hostname": "blog.tcweb.org",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1620639788,
  "expire_in": 157680000,
  "prev": "7dd9744ca533c8208ed3049927fa4c12010efa2c18c8c5bd9a138ef0e8bf2501",
  "seqno": 9,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----

owGtkntQVFUcx3eRh2LEKoKORcHlrQTn3rt3997lNQgOCGWGBo5gy32cu9xxd+92
d1kgBqmJETSYGmlKUMTiESRjINoApjEVUOr2IEIRKCVjYAJzKkGNR3cZ/a8/O/+c
Ob/z/X7O7/c7v3e8Vik8lQFXJ+J3T97erLwyzShyaxRpxQgjckWIrhg5AFc2aOSg
1aY/IHCIDgEoQFmSBATEWJrR0pwWAI4iAEtqtThOYARLYBSg1SiKc4AGlBolcFTN
E1BNogzHMRAACGgkEuEFswFKFkkw22QsxXMcSQNOxvIUTstmlGNIHsMAy0IMhwQO
NVCNOo15otXpkJNjaCuMEkQ5Jh/0K+n9h/5/zjt/BSczMABYlGJoDgUciRKQwgDD
ozjKYTxKOYVWKJlpE5TVNtZI2wUoISWRiBy1Cyx0NtZZySMFYxQNUTa2ADJRomSQ
3RZJtImsaJTv8mw2i1Xn9NqKLE6xrNI/wugZwczJnZQddihZBdGM6FBZydoEJxfV
YECDU1qSjERgoUWQoF5wKgithgTycr4D7TJSy3GUdp9azdIEjrMkBkjI4UBNUZiW
p9Usisn9gzyNsSjJkizBcJT8RyTkASQZHiMAijgre9UsIjpKzpM2yEyrYDDTtnwJ
IiWeh11wV4XSU+HvF+haP37E65Xo6g/i/Lv+fDx9bi7O0VN4rlE9jiQUeS8FGVqT
m2PDTr6WNxJ+NsRY1ScsdQ+PhJ2YbKxwTGtc93s9I4SGbMrc+WBV4K1+PHkwrv67
wvJlNLFHM5iXtOPW0zn38vOH52I62OptjsL6rLNz1ddmp3zvdodFJDyV/kRLxB1r
qqbjvdmboH2jONk8MpY1lzn94EZjUaHL2rbijaYbfaq3HANvv/uXfSxYO+Rz+lBv
0PlEn9ErCa0ZUX4P6cqhmeKvUo7Ht1Ueu863guiM4KMZjovfDw4W/Dw+H2w6l7Jn
g8HbMSb6vXziYvOzl8XGlnjd2g0jy4pp5eLekJCupNoP3y8IGAm921QX6e9xsHSx
Kno5fG9/7ke/3KHCUjujzx8MbUox/SNtXX9OesF9vDItfPi5AV2AhHm4HXdtcHhT
W2Zgdm8iVfriQlxMRMu4MhTZqrx/qazN52RDctYO0Gg6U9rx0h63+dDTF5Lrvt79
e7mvY0FR8WtGYEWKR/u3PT9MzK8PnDElXLe4Hyn55nnl7T9iVER2xFXjp6N61f2m
n2p7h44+ua5wdFfnTsfhQ+iCKucLaU33tZp1vTf5z8qCLi0OTrWEfHmqxycty+XU
rvTtx7K3E1U9ZVs+njrz9/4l967fJvbF1r3Z0E9suhDjcTmpLyY1c7wp1t7OrI5l
IspHD31SY379ngFbPeub/sbnbK6x1m2gsLpT1fjj5q5wrD/xYU7fvw==
=5Syo
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/tclavier

==================================================================

